﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using filecopyapp.Interfaces;
using System.IO;
using System.Windows;
using System.Windows.Threading;

namespace filecopyapp.Implementations
{
    public class SimpleCopier : ICopier
    {
        public IDictionary dictionary { get; set; }
        public CopyWindow copyWindow { get; set; }
        private readonly MainWindow.RefreshDestination _refreshDestination;

        public SimpleCopier(MainWindow.RefreshDestination refresh)
        {
            _refreshDestination = refresh;
            dictionary = new Dictionary<string, string>();
            copyWindow = new CopyWindow {CopyList = {ItemsSource = dictionary.Values}};
        }

        public void CopyFile(string originPath, string destinationPath)
        {            
            File.Copy(originPath, destinationPath + "\\" + Path.GetFileName(originPath), true);
        }

        public async void CopyFileAsync(string originPath, string destinationPath)
        {
            if (!copyWindow.IsVisible)
            {
                copyWindow.Show();
            }

            var fileN = Path.GetFileName(originPath);
            if (dictionary.Contains(fileN))
            {
                return;
            }

            dictionary.Add(fileN, originPath + " => " + destinationPath);
            copyWindow.CopyList.Items.Refresh();

            await Task.Run(() => CopyFile(originPath, destinationPath));

            dictionary.Remove(fileN);
            copyWindow.CopyList.Items.Refresh();

            _refreshDestination(null, null);

            if (dictionary.Count == 0)
            {
                copyWindow.Hide();
            }
        }
    }
}
