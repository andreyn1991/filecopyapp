﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using filecopyapp.Implementations;
using filecopyapp.Interfaces;
using System.IO;
using WPFFolderBrowser;

namespace filecopyapp
{
    public partial class MainWindow : Window
    {
        private readonly ICopier _copier;

        public delegate void RefreshDestination(object sender, TextChangedEventArgs e);
        public MainWindow()
        {
            InitializeComponent();
            _copier = new SimpleCopier(TbDestinationPath_TextChanged);
        }

        private void BOriginPath_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new WPFFolderBrowserDialog("Выберите откуда копировать")
            {
                InitialDirectory = TbOriginPath.Text
            };
            var result = (bool) dialog.ShowDialog();
            if (!result)
                return;

            TbOriginPath.Text = dialog.FileName;
        }

        private void BDestinationPath_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new WPFFolderBrowserDialog("Выберите куда копировать")
            {
                InitialDirectory = TbDestinationPath.Text
            };
            dialog.ShowDialog();

            if (dialog.FileName == null)
                return;

            TbDestinationPath.Text = dialog.FileName;
        }

        private void BCopy_Click(object sender, RoutedEventArgs e)
        {
            if (OriginList.SelectedItem == null)
            {
                MessageBox.Show("Выберите файл!");
                return;
            }

            if (!Directory.Exists(TbDestinationPath.Text))
            {
                MessageBox.Show("Выберите путь сохранения файлов!");
                return;
            }

            _copier.CopyFileAsync(OriginList.SelectedItem.ToString(), TbDestinationPath.Text);
        }

        private void TbOriginPath_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (Directory.Exists(TbOriginPath.Text))
            {
                OriginList.ItemsSource = Directory.GetFiles(TbOriginPath.Text);
            }            
        }

        private void TbDestinationPath_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (Directory.Exists(TbDestinationPath.Text))
            {
                DestinationList.ItemsSource = Directory.GetFiles(TbDestinationPath.Text);
            }
        }

        private void MyMainWindow_Closed(object sender, EventArgs e)
        {
            Application.Current.Shutdown();
        }
    }
}
