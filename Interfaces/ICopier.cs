﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace filecopyapp.Interfaces
{
    public interface ICopier
    {
        IDictionary dictionary { get; set; }
        CopyWindow copyWindow { get; set; }
        void CopyFile(string originPath, string destinationPath);
        void CopyFileAsync(string originPath, string destinationPath);
    }
}
